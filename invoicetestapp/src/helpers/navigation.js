/**
 * This constants is used for the screen navigation
 */
import {CommonActions} from '@react-navigation/native';

let _container;

function setContainer(container) {
  _container = container;
}

//This function is used for the remove all previous stack screen and goto new screen
function navigate(routeName, params) {
  let currentRoute = _container.getCurrentRoute();
  if (currentRoute && currentRoute.name == routeName) {
  } else {
    _container.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name: routeName, params}],
      }),
    );
  }
}

export default {
  setContainer,
  navigate,
};
