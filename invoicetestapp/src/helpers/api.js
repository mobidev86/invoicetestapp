/**
 * This constants is used for the api calling
 */
import axios from 'axios';
import {logout} from '../redux/reducer';
import {store} from '../redux/store/store';
import NavigationService from './navigation';

//server uri
export const apiRoot = 'https://sandbox.101digital.io';

//client_id
export const client_id = 'oO8BMTesSg9Vl3_jAyKpbOd2fIEa';

//client_id
export const client_secret = '0Exp4dwqmpON_ezyhfm0o_Xkowka';

//api response type
export const actions = {load: '_LOAD', success: '_SUCCESS', error: '_ERROR'};

//make reducer action based on type
export const getAction = (action, type) => {
  if (type === actions.load) {
    return action + actions.load;
  } else if (type === actions.success) {
    return action + actions.success;
  } else {
    return action + actions.error;
  }
};
//api timeout for axios
const timeout = 100000;

//configure axios for api calling
export const client = axios.create({
  baseURL: apiRoot,
  timeout: timeout,
  headers: {'Content-Type': 'application/x-www-form-urlencoded'},
});
let isLogoutCalled = false;

export function setIsLogout(isLogout) {
  isLogoutCalled = isLogout;
}

// Add a request interceptor
client.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    let user = store.getState().login.user;
    let basicAuth = user && user.access_token ? user.access_token : null;
    let orgToken = user && user.memberships ? user.memberships[0].token : null;

    if (basicAuth) {
      config.headers['authorization'] = `Bearer ${basicAuth}`;
      // config.headers['org-token'] = `${orgToken}`;
    } else {
      // useNavigation().navigate("SignIn");
    }
    return config;
  },
  function (error) {
    // Do something with request error
    extras = {
      request: error.request,
    };

    return Promise.reject(error);
  },
);

// Add a response interceptor for the log api calling
client.interceptors.response.use(
  function (response) {
    // Do something with response data
    return response.data;
  },
  async function (error) {
    // Do something with response error
    if (
      error &&
      error.response &&
      error.response.status == 401 &&
      !isLogoutCalled
    ) {
      let data =
        error && error.response && error.response.data
          ? error.response.data
          : null;
      setIsLogout(true);

      if (error.response.status == 401) {
        await store.dispatch(logout());
      } else if (data && data.errorCode && data.errorCode === 101) {
        await store.dispatch(logout());
        NavigationService.navigate('Login');
        window.onError(error);
      } else if (data && data.message && data.message == 'Unauthorized Token') {
        let user = store.getState().login.user;
        NavigationService.navigate('Login');
      } else {
        await store.dispatch(logout());
        NavigationService.navigate('Login');
      }
      setTimeout(() => {
        setIsLogout(false);
      }, 5000);
    }

    if (error && error.response && error.response.data) {
      return Promise.resolve(error.response.data);
    } else {
      return Promise.reject(error);
    }
  },
);
