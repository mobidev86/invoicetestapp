/**
 * This is the home navigation file for setup all screen navigation which are used in flow
 */

import {createNativeStackNavigator} from '@react-navigation/native-stack';
import CreateInvoiceScreen from '../screens/CreateInvoiceScreen';
import HomeScreen from '../screens/HomeScreen';

function HomeStack() {
  const Stack = createNativeStackNavigator();
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="CreateInvoice" component={CreateInvoiceScreen} />
    </Stack.Navigator>
  );
}

export default HomeStack;
