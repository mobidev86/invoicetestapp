/**
 * This is the main file for setup all project screen and structures
 */

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from '../screens/HomeScreen';
import LoginScreen from '../screens/LoginScreen';
import {useSelector} from 'react-redux';
import HomeStack from './HomeStack';
import AuthStack from './AuthStack';

function AppNavigator() {
  const user = useSelector(state => state.login.user);
  const Stack = createNativeStackNavigator();
  return (
    <NavigationContainer>
      {user ? <HomeStack /> : <AuthStack />}
    </NavigationContainer>
  );
}

export default AppNavigator;

