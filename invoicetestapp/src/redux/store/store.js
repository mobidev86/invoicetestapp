import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistStore, persistReducer} from 'redux-persist';
import {configureStore} from '@reduxjs/toolkit';
import logger from 'redux-logger';

// Imports: Redux
import thunk from 'redux-thunk';
import rootReducer from '../reducer';

// Middleware: Redux Persist Config
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['login'], //This for save data into local storage for specific reducer
};

// Middleware: Redux Persist Persisted Reducer
const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware(
      {
        thunk: thunk,
        serializableCheck: false,
        immutableCheck: false,
      },
      logger,
    ),
});

// Middleware: Redux Persist Persister
let persistor = persistStore(store);

// Exports
export {store, persistor};
