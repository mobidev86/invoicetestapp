/**
 * This reducer is used for user authentication flow and onboarding flow
 */
export const LOGIN_USER = 'LOGIN_USER';
export const CLEAN_LOGIN_DATA = 'CLEAN_LOGIN_DATA';
export const UPDATE_USER_DATA = 'UPDATE_USER_DATA';
import {actions, client, client_id, getAction} from '../../helpers/api';
import {client_secret} from '../../helpers/api';

//Below are the reducers is used for the auth flow
const initialState = {
  loading: false,
  user: null,
};

//store api data into reducer based on action type
export default function (state = initialState, action) {
  switch (action.type) {
    case getAction(LOGIN_USER, actions.load):
      return {
        ...state,
        loading: true,
      };
    case getAction(LOGIN_USER, actions.success):
      return {
        ...state,
        loading: false,
        user: action.payload,
      };
    case getAction(LOGIN_USER, actions.error):
      return {
        ...state,
        loading: false,
      };
    case getAction(UPDATE_USER_DATA, actions.success):
      let userData = state.user;
      let newData = {...userData, ...action.payload};
      return {
        ...state,
        user: newData,
      };
    case CLEAN_LOGIN_DATA:
      return {
        ...initialState,
      };

    default:
      return state;
  }
}

//This function is used for the login api
export const login = data => dispatch =>
  new Promise(function (resolve, reject) {
    dispatch({
      type: getAction(LOGIN_USER, actions.load),
    });
    client
      .post(
        `/token?client_id=${client_id}&client_secret=${client_secret}`,
        data,
      )
      .then(response => {
        if (response) {
          dispatch({
            type: getAction(LOGIN_USER, actions.success),
            payload: response,
          });
        } else {
          dispatch({
            type: getAction(LOGIN_USER, actions.error),
          });
        }
        resolve(response);
      })
      .catch(err => {
        dispatch({
          type: getAction(LOGIN_USER, actions.error),
        });
        reject(err);
      });
  });

//This function is used for the get user profile data
export const getProfile = () => dispatch =>
  new Promise(function (resolve, reject) {
    client
      .get(`/membership-service/1.2.0/users/me`)
      .then(response => {
        dispatch({
          type: getAction(UPDATE_USER_DATA, actions.success),
          payload: response.data,
        });
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });
