/**
 * This reducer is used for user authentication flow and onboarding flow
 */
export const INVOICE_LIST = 'INVOICE_LIST';
import {actions, client, client_id, getAction} from '../../helpers/api';
import {client_secret} from '../../helpers/api';

//Below are the reducers is used for the auth flow
const initialState = {
  loading: false,
  invoiceList: null,
};

//store api data into reducer based on action type
export default function (state = initialState, action) {
  switch (action.type) {
    case getAction(INVOICE_LIST, actions.load):
      return {
        ...state,
        loading: true,
      };
    case getAction(INVOICE_LIST, actions.success):
      return {
        ...state,
        loading: false,
        invoiceList: action.payload,
      };
    case getAction(INVOICE_LIST, actions.error):
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
}

//This function is used for get invoice list
export const getInvoices = (keyword, order) => dispatch =>
  new Promise(function (resolve, reject) {
    client
      .get(
        `/invoice-service/1.0.0/invoices?pageNum=1&pageSize=10&dateType=INVOICE_DATE&sortBy=CREATED_DATE&ordering=${order}&keyword=${keyword}`,
      )
      .then(response => {
        dispatch({
          type: getAction(INVOICE_LIST, actions.success),
          payload: response.data,
        });
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });

//This function is used for get invoice list
export const createInvoice = () => dispatch =>
  new Promise(function (resolve, reject) {
    client
      .post(`https://sandbox.101digital.io/invoice-service/2.0.0/invoices`)
      .then(response => {
        console.log('response', response);
        dispatch({
          type: getAction(INVOICE_LIST, actions.success),
          payload: response.data,
        });
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });
