/**
 * This main reducer file is used for the link multiple reducer into single file
 */
import {combineReducers} from 'redux';
import login, {CLEAN_LOGIN_DATA} from './login';
import invoice from './invoice';

import {client} from '../../helpers/api';

//combine reducer for the generalize code
const allReducers = combineReducers({
  login: login,
  invoice: invoice,
});

//This logout function is used for the logout user from the app and clear all the data as well
export const logout = user => (dispatch, getState) =>
  new Promise(async function (resolve, reject) {
    await dispatch({
      type: CLEAN_LOGIN_DATA,
    });
    resolve();
  });

const rootReducer = (state, action) => {
  return allReducers(state, action);
};

export default rootReducer;
