/**
 * This component is used for the main parent view for the app for each screen.
 */
import * as React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {StyleSheet} from 'react-native';
import PropTypes from 'prop-types';

function Header(props) {
  return (
    <View>
      <View style={[styles.mainView, props.style ? props.style : {}]}>
        <View style={styles.dataContainer}>
          {props.title ? (
            <TouchableOpacity
              activeOpacity={props.onTitlePress ? 0.2 : 1}
              onPress={props.onTitlePress}>
              <Text
                errorBold
                style={{
                  ...props.titleStyle,
                }}>
                {props.title ? props.title : ''}
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>
      </View>
    </View>
  );
}

// styles for component
const styles = StyleSheet.create({
  mainView: {
    flexDirection: 'row',
    paddingVertical: 0,
    alignItems: 'center',
    height: 55,
  },
  text: {
    textAlign: 'center',
    fontSize: 16,
  },
  dataContainer: {flex: 1, justifyContent: 'center', alignItems: 'center'},
});
//Below are the props for this component.
Header.propTypes = {
  title: PropTypes.string,
  onBackPress: PropTypes.func,
  titleStyle: PropTypes.object,
  style: PropTypes.object,
};
export default Header;
