/**
 * This component is used for the main parent view for the app for each screen.
 */
import * as React from 'react';
import {ActivityIndicator, Text, TouchableOpacity, View} from 'react-native';
import PropTypes from 'prop-types';
import Colors from '../constant/Colors';
import {StyleSheet} from 'react-native';

function Button(props) {
  return (
    <View style={[styles.mainView, props.style ? props.style : {}]}>
      <TouchableOpacity
        disabled={props.disabled}
        onPress={props.onPress}
        style={props.disabled ? styles.disableButtonView : styles.buttonView}>
        <View style={styles.dataContainer}>
          {props.loading && (
            <ActivityIndicator color={'white'} style={styles.buttonLoader} />
          )}
          <Text style={styles.buttonText}>{props.children}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}
// style for component
const styles = StyleSheet.create({
  mainView: {
    width: '70%',
    alignSelf: 'center',
    borderRadius: 25,
    marginBottom: 20,
  },
  buttonText: {
    color: Colors.whiteBG,
    textAlign: 'center',
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: '500',
  },
  buttonView: {
    alignItem: 'center',
    justifyContent: 'center',
    height: 45,
    borderRadius: 25,
    backgroundColor: Colors.colorBlack,
  },
  disableButtonView: {
    alignItem: 'center',
    justifyContent: 'center',
    height: 45,
    borderRadius: 25,
    opacity: 0.5,
    backgroundColor: Colors.colorBlack,
  },
  buttonLoader: {position: 'absolute', left: 20, alignSelf: 'center'},
  dataContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
//Below are the props for this component.
Button.propTypes = {
  loading: PropTypes.bool,
  disabled: PropTypes.bool,
  normal: PropTypes.bool,
  style: PropTypes.object,
  onPress: PropTypes.func,
};
export default Button;
