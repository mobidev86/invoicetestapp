/**
 * This component for the display blur view like in the Overview screen for the view all accounts button.
 */
import * as React from 'react';
import {View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Text} from '..';
import Colors from '../../constant/Colors';
import DeviceInfo from 'react-native-device-info';

export default function NoInternet(props) {
  const insect = useSafeAreaInsets();

  return (
    <View
      style={{
        backgroundColor: '#f56642',
        height: 30,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: DeviceInfo.hasNotch() ? insect.top : 0,
      }}>
      <Text medium style={{color: Colors.whiteBG}}>
        {'No internet connected!!'}
      </Text>
    </View>
  );
}
