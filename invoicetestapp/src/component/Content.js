/**
 * This component is used for the add scroll view in the whole app for the each screen.
 */
import * as React from 'react';
import {ScrollView, View} from 'react-native';

export default function Content(props) {
  return (
    <View style={[{flex: 1}, props.style ? props.style : {}]}>
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        nestedScrollEnabled
        bounces={true}
        showsVerticalScrollIndicator={props.showsVerticalScrollIndicator}
        showsHorizontalScrollIndicator={props.showsHorizontalScrollIndicator}
        onScroll={props.onScroll}
        onMomentumScrollEnd={props.onMomentumScrollEnd}
        contentContainerStyle={props.contentContainerStyle}
        style={[{flex: 1}, props.scrollViewStyle ? props.scrollViewStyle : {}]}>
        {props.children}
      </ScrollView>
    </View>
  );
}
