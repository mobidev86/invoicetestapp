/**
 * This component is used for the main parent view for the app for each screen.
 */
import * as React from 'react';
import {KeyboardAvoidingView, Platform, View} from 'react-native';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';
import Colors from '../constant/Colors';

export default function Container(props) {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: props.transparent
          ? Colors.transparent
          : Colors.whiteBG,
      }}
      edges={props.noSafeArea ? [] : ['left', 'right', 'top']}>
      <KeyboardAvoidingView
        style={{flex: 1}}
        behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
        enabled={Platform.OS == 'android' ? true : true}
        keyboardVerticalOffset={-20}>
        <View
          style={{
            backgroundColor: props.transparent
              ? Colors.transparent
              : Colors.whiteBG,
            flex: 1,
          }}>
          {props.children}
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}
