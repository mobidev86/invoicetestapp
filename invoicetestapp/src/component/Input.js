/**
 * This component is used for the main parent view for the app for each screen.
 */
import * as React from 'react';
import {TextInput, View} from 'react-native';
import {StyleSheet} from 'react-native';
import PropTypes from 'prop-types';

function Input(props) {
  return (
    <View>
      <View style={[styles.mainView, props.style ? props.style : {}]}>
        <TextInput
          style={styles.input}
          placeholder={props.placeholder}
          editable={props.editable}
          keyboardType={props.keyboardType ? props.keyboardType : 'default'}
          placeholderTextColor={
            props.placeholderTextColor ? props.placeholderTextColor : 'gray'
          }
          returnKeyType={props.returnKeyType ? props.returnKeyType : 'default'}
          onSubmitEditing={e => {
            if (props.onSubmitEditing) {
              props.onSubmitEditing();
            }
          }}
          secureTextEntry={props.secureTextEntry ? true : false}
          value={props.value}
          onChangeText={newValue => props.onChangeText(newValue)}
          maxLength={props.maxLength}
          autoFocus={props.autoFocus}
        />
      </View>
    </View>
  );
}

// styles for component
const styles = StyleSheet.create({
  input: {
    width: '80%',
    height: 45,
    backgroundColor: '#fff',
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 10,
    fontSize: 16,
  },
  mainView: {
    flexDirection: 'row',
    paddingVertical: 0,
    justifyContent: 'center',
    alignItems: 'center',
    height: 55,
  },
});
//Below are the props for this component.
Input.propTypes = {
  placeholder: PropTypes.string,
  style: PropTypes.object,
  onChangeText: PropTypes.func,
  value: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  autoFocus: PropTypes.bool,
  keyboardType: PropTypes.string,
  returnKeyType: PropTypes.string,
  onSubmitEditing: PropTypes.func,
  maxLength: PropTypes.number,
  secureTextEntry: PropTypes.bool,
  editable: PropTypes.bool,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
};
export default Input;
