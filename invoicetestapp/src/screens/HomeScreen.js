/**
 * This screen is used for display invoice listing
 */
import {Text, View} from 'react-native';
import {useSelector} from 'react-redux';
import {useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {getInvoices} from '../redux/reducer/invoice';
import Container from '../component/Container';
import {Content, Header, Input} from '../component';
import {StyleSheet} from 'react-native';
import Colors from '../constant/Colors';
import {TouchableOpacity} from 'react-native';
import {useIsFocused} from '@react-navigation/native';

function HomeScreen(props) {
  const invoiceList = useSelector(state => state.invoice.invoiceList);
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  const [search, setSearch] = useState('');
  const [order, setOrder] = useState('DESCENDING');

  useEffect(() => {
    getData();
  }, [isFocused]);

  //when user enter text in search this function will called
  useEffect(() => {
    getData();
  }, [search]);

  //when change order of list this function will called
  useEffect(() => {
    getData();
  }, [order]);

  //function get invoice list from api
  async function getData() {
    let res = await dispatch(getInvoices(search, order));
  }

  //redirect to create invoice screen
  function createInvoice() {
    props.navigation.navigate('CreateInvoice');
  }

  //this function is used for change order of list
  function changeOrder() {
    if (order === 'DESCENDING') {
      setOrder('ASCENDING');
    } else {
      setOrder('DESCENDING');
    }
  }

  // this will render all UI component on screen
  return (
    <Container>
      <Header title="Home" />

      <Content>
        <Input
          placeholder={'Search'}
          value={search}
          onChangeText={value => {
            setSearch(value);
          }}
        />
        {invoiceList &&
          invoiceList.map((item, index) => {
            return (
              <View key={index}>
                <View style={styles.dataContainer}>
                  <Text>Invoice Number: {item.invoiceNumber}</Text>
                  <Text>InvoiceId: {item.invoiceId}</Text>
                  <Text>Invoice Date: {item.invoiceDate}</Text>
                </View>
              </View>
            );
          })}
      </Content>
      <View style={styles.floatingConatiner}>
        <TouchableOpacity style={styles.floatingButton} onPress={createInvoice}>
          <Text style={styles.floatingIcon}>+</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.floatingButton} onPress={changeOrder}>
          <Text style={styles.floatingIcon}>
            {order === 'ASCENDING' ? '↑' : '↓'}
          </Text>
        </TouchableOpacity>
      </View>
    </Container>
  );
}
// styles for component
const styles = StyleSheet.create({
  dataContainer: {
    marginHorizontal: 20,
    paddingVertical: 10,
    borderBottomWidth: 0.5,
  },
  floatingButton: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginVertical: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.colorBlack,
  },
  floatingConatiner: {
    position: 'absolute',
    bottom: 20,
    right: 20,
  },
  floatingIcon: {
    fontSize: 30,
    alignSelf: 'center',
    color: Colors.whiteBG,
  },
});
export default HomeScreen;
