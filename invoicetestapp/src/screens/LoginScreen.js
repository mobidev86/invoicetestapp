/**
 * This screen is used for login user with passcode
 */
import {useState} from 'react';
import {Button, Container, Content, Header} from '../component';
import Input from '../component/Input';
import {useDispatch} from 'react-redux';
import {Alert, Text, View} from 'react-native';
import {StyleSheet} from 'react-native';
import {client_id, client_secret} from '../helpers/api';
import {getProfile, login} from '../redux/reducer/login';
import queryString from 'query-string';
import {useSelector} from 'react-redux';
import {CommonActions} from '@react-navigation/native';

function LoginScreen(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  // this function is called when user click on login Button
  async function onLoginButtonPress() {
    setLoading(true);
    let data = {
      client_id: 'oO8BMTesSg9Vl3_jAyKpbOd2fIEa',
      client_secret: '0Exp4dwqmpON_ezyhfm0o_Xkowka',
      grant_type: 'password',
      scope: 'openid',
      username: email,
      password: password,
    };
    const request = queryString.stringify(data);
    //login Api call
    let res = await dispatch(login(request));
    console.log('res', res);
    if (res?.error === 'invalid_grant') {
      Alert.alert(res?.error_description);
    } else {
      //get profilt api call
      let user = await dispatch(getProfile(request));

      setLoading(false);
      //navigate to invoiceList screen
      props.navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{name: 'Home'}],
        }),
      );
    }
  }

  // this will render all UI component on screen
  return (
    <Container>
      <Header title="Login" />
      <Content contentContainerStyle={{flex: 1}}>
        <View style={styles.formContainer}>
          <Input
            placeholder={'Email'}
            autoFocus={true}
            value={email}
            onChangeText={value => {
              setEmail(value);
            }}
            keyboardType={'email-address'}
          />
          <Input
            placeholder={'Password'}
            value={password}
            onChangeText={value => {
              setPassword(value);
            }}
            secureTextEntry={true}
          />
        </View>
        <Button
          loading={loading}
          disabled={!email || !password || loading} //disable login button when email or password is not filled and Api calling is in progress
          onPress={() => onLoginButtonPress()}>
          {'login'}
        </Button>
      </Content>
    </Container>
  );
}
// styles for component
const styles = StyleSheet.create({
  formContainer: {
    height: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default LoginScreen;
