/**
 * This screen is used for create new invoice
 */
import {useState} from 'react';
import {Button, Container, Content, Header} from '../component';
import Input from '../component/Input';
import {useDispatch} from 'react-redux';
import {Text, View} from 'react-native';
import {StyleSheet} from 'react-native';
import {createInvoice} from '../redux/reducer/invoice';
import {useSelector} from 'react-redux';

function CreateInvoiceScreen(props) {
  const [email, setEmail] = useState('dung+octopus4@101digital.io');
  const [password, setPassword] = useState('Abc@123456');
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const invoiceList = useSelector(state => state.invoice.invoiceList);
  //store data of bankAccount
  const [bankAccountData, setBankAccountData] = useState({
    bankId: Math.floor(Math.random() * 1000) + 1,
    sortCode: '',
    accountNumber: '',
    accountName: '',
  });

  //store data of customer
  const [customerData, setCustomerData] = useState({
    firstName: '',
    lastName: '',
  });

  //store contact of customer
  const [contactData, setContactData] = useState({
    email: '',
    mobileNumber: '',
  });

  //store address of customer
  const [addressData, setAddressData] = useState({
    premise: '',
    countryCode: '',
    county: '',
    postcode: '',
    city: '',
  });

  // this function is called when user click on login Button
  async function onLoginButtonPress() {
    let data = invoiceList[0];
    let arrayOfInvoice = [];
    let addresses = [];
    addresses.push(addressData);
    let customer = {
      customerData,
      contact: contactData,
      addresses: addresses,
    };
    let newInvoice = {
      bankAccount: bankAccountData,
      customer: customer,
      invoiceReference: `#${Math.floor(Math.random() * 1000) + 1}`,
      invoiceNumber: `INV${Math.floor(Math.random() * 1000) + 1}`,
      currency: 'GBP',
      invoiceDate: new Date(),
      dueDate: new Date(),
      description: 'Invoice is issued to Akila Jayasinghe',
    };
    console.log('data', newInvoice);

    arrayOfInvoice.push(newInvoice);
    let list = {listOfInvoices: arrayOfInvoice};
    // let res = await dispatch(createInvoice(list));
    props.navigation.goBack();
  }

  //function for render form text input
  const renderInput = (placeHolder, text, setText, data, setData) => {
    return (
      <Input
        placeholder={placeHolder}
        autoFocus={true}
        value={text}
        onChangeText={value => {
          var form = data;
          data[setText] = value;
          setData(() => {
            return {...form};
          });
        }}
      />
    );
  };
  // this will render all UI component on screen
  return (
    <Container>
      <Header title="Create Invoice" />
      <Content>
        <Text style={styles.titleText}>Account Data</Text>
        {renderInput(
          'sortCode',
          bankAccountData.sortCode,
          'sortCode',
          bankAccountData,
          setBankAccountData,
        )}
        {renderInput(
          'accountNumber',
          bankAccountData.accountNumber,
          'accountNumber',
          bankAccountData,
          setBankAccountData,
        )}
        <Text style={styles.titleText}>Customer Data</Text>
        {renderInput(
          'firstName',
          customerData.firstName,
          'firstName',
          customerData,
          setCustomerData,
        )}
        {renderInput(
          'lastName',
          customerData.lastName,
          'lastName',
          customerData,
          setCustomerData,
        )}
        {renderInput(
          'email',
          contactData.email,
          'email',
          contactData,
          setContactData,
        )}
        {renderInput(
          'mobileNumber',
          contactData.mobileNumber,
          'mobileNumber',
          contactData,
          setContactData,
        )}
        {renderInput(
          'premise',
          addressData.premise,
          'premise',
          addressData,
          setAddressData,
        )}
        {renderInput(
          'countryCode',
          addressData.countryCode,
          'countryCode',
          addressData,
          setAddressData,
        )}
        {renderInput(
          'postcode',
          addressData.postcode,
          'postcode',
          addressData,
          setAddressData,
        )}
        {renderInput(
          'county',
          addressData.county,
          'county',
          addressData,
          setAddressData,
        )}
        {renderInput(
          'city',
          addressData.city,
          'city',
          addressData,
          setAddressData,
        )}
        <Button
          loading={loading}
          style={{marginTop: 20}}
          disabled={!email || !password || loading} //disable login button when email or password is not filled and Api calling is in progress
          onPress={() => onLoginButtonPress()}>
          {'Create'}
        </Button>
        <Button
          loading={loading}
          disabled={!email || !password || loading} //disable login button when email or password is not filled and Api calling is in progress
          onPress={() => props.navigation.goBack()}>
          {'Back'}
        </Button>
      </Content>
    </Container>
  );
}
// styles for component
const styles = StyleSheet.create({
  titleText: {marginLeft: 10, fontSize: 15, marginVertical: 10},
});
export default CreateInvoiceScreen;
