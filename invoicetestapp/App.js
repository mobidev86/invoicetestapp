/**
 * This is the main file for setup all project screen and structures
 */
import React, {useEffect, useRef, useState} from 'react';
import {AppState, Platform, StatusBar, StyleSheet} from 'react-native';

import {persistor, store} from './src/redux/store/store';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/es/integration/react';
import AppNavigator from './src/navigation/AppNavigator';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import NetInfo from '@react-native-community/netinfo';
import Colors from './src/constant/Colors';
import {NoInternet} from './src/component';

export default function App() {
  const [isNetworkAvailable, setIsNetworkAvailable] = useState(); //This for capture internet status
  async function checkNetwork() {
    const response = await NetInfo.fetch();
    setIsNetworkAvailable(response.isConnected);
  }

  useEffect(() => {
    let unsubscribe = NetInfo.addEventListener(state => {
      checkNetwork();
    });
    return unsubscribe;
  }, []);

  return (
    <SafeAreaProvider>
      <StatusBar
        translucent
        barStyle={Platform.OS == 'ios' ? 'dark-content' : 'dark-content'}
        backgroundColor={
          Platform.OS == 'ios' ? Colors.whiteBG : Colors.transparent
        }
      />

      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <AppNavigator />
        </PersistGate>
      </Provider>
      {!isNetworkAvailable && <NoInternet />}
    </SafeAreaProvider>
  );
}
